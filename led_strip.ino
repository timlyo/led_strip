#include <NewPing.h>
#include <FastLED.h>

const int DATA_PIN = 6;
const int PING_PIN = 7;
const int MAX_DISTANCE = 60;
const int NUM_LEDS = 50;
const float COEFFICIENT = 5.20408163265;

NewPing sonar(PING_PIN, PING_PIN, MAX_DISTANCE);

CRGB leds[NUM_LEDS];

void set_led(unsigned const int index, CRGB colour, unsigned int forward=0, unsigned int backwards=0);

class EffectController{
public:
	void light_up(){
		for(int x = 0; x < NUM_LEDS; x++){
			set_led(x, CRGB(255, 255, 255)); 
		}
	FastLED.show();
	}

	virtual void wait(){
		delay(2000);
	}

	void turn_off(){
		for(int x = 0; x < NUM_LEDS; x++){
			set_led(x, CHSV(0, 0, 0));
		}
	FastLED.show();
	}

};

class RainbowController: public EffectController{
public:
	void light_up(){
		for(int x = 0; x < NUM_LEDS; x++){
			set_led(x, CHSV(x * COEFFICIENT, 255, 255), 4);
			FastLED.show();
			delay(30);
		}
	}

	void turn_off(){
		for(int x = 0; x < NUM_LEDS; x++){
			set_led(x, CHSV(0, 0, 0));
			FastLED.show();
			delay(50);
		}
	}
};

void setup() {
	Serial.begin(9600);
	FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop() {
	unsigned int dist = sonar.ping_cm();

	RainbowController controller = RainbowController();

	if(dist > 3){
		controller.light_up();
		controller.wait();
		controller.turn_off();
	}

	FastLED.show();
	delay(50);
}

void set_led(unsigned const int index, CRGB colour, unsigned int forward=0, unsigned int backwards=0){
	if(index >= NUM_LEDS){
		return;
	}

	leds[index] = colour;
	if(forward > 0)
		set_led(index + 1, colour/2, forward-1, 0);

	if(backwards > 0)
		set_led(index - 1, colour/2, 0, backwards-1);
}


